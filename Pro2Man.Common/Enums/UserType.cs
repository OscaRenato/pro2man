﻿namespace Pro2Man.Common.Enums
{
    public enum UserType
    {
        Admin,
        Maintenance,
        Production
    }
}
