﻿using System.ComponentModel.DataAnnotations;

namespace Pro2Man.Common.Models
{
    public class EmailRequest
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
