﻿using Pro2Man.Common.Enums;
using System.Collections.Generic;

namespace Pro2Man.Common.Model
{
    public class OperatorResponse
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string CodeOperator { get; set; }

        public string Dni { get; set; }    

        public UserType Occupation { get; set; }

        public string Photo { get; set; }

        public string FullName => $"{Name} {LastName}";

        public string FullPhoto => string.IsNullOrEmpty(Photo)
            ? "https://pro2manwebservice.azurewebsites.net/images/Users/NoImage.png"
            : $"https://pro2manwebservice.azurewebsites.net/{Photo.Substring(1)}";

        public ICollection<ReportResponse> Reports { get; set; }

        public ICollection<WorkOrderResponse> WorkOrders { get; set; }
    }
}
