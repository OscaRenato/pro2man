﻿using System.Collections.Generic;

namespace Pro2Man.Common.Model
{
    public class MachineResponse
    {
        public int Id { get; set; }

        public string CodeMachine { get; set; }

        public string NameMachine { get; set; }

        public string Photo { get; set; }

        public string FullPhoto => string.IsNullOrEmpty(Photo)
            ? "https://pro2manwebservice.azurewebsites.net/images/Users/NoImage.png"
            : $"https://pro2manwebservice.azurewebsites.net{Photo.Substring(1)}";
    }
}
