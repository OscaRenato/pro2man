﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Pro2Man.Common.Models
{
    public class ReportRequest
    {
        [MaxLength(150, ErrorMessage = "El campo {0} no puede tener más de {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string ObservPro { get; set; }               

        [Display(Name = "Codigo Equipo")]
        [MaxLength(6, ErrorMessage = "El campo {0} no puede tener más de {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string CodeMachine { get; set; }

        public Guid UserId { get; set; }

        public string IdOperator { get; set; }
    }
}
