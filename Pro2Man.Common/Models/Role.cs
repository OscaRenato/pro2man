﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pro2Man.Common.Models
{
    public class Role
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
