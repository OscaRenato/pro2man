﻿using System;

namespace Pro2Man.Common.Model
{
    public class ReportResponse
    {
        public int Id { get; set; }

        public string ObservPro { get; set; }

        public string Status { get; set; }

        public DateTime StarReport { get; set; }        

        public DateTime StartReportLocal => StarReport.ToLocalTime();

        public WorkOrderResponse WorkOrder { get; set; }

        public MachineResponse Machine { get; set; }
    }
}
