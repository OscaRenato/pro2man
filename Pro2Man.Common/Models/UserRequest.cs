﻿using System.ComponentModel.DataAnnotations;

namespace Pro2Man.Common.Models
{
    public class UserRequest
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Dni { get; set; }

        [Required]
        public string CodeOperator { get; set; }

        [Required]
        public int UserTypeId { get; set; }

        [Required]
        public string Phone { get; set; }

        [Required]
        public string Email { get; set; }                     

        [Required]
        [StringLength(20, MinimumLength = 6)]
        public string Password { get; set; }        

        public byte[] PictureArray { get; set; }

        public string PasswordConfirm { get; set; }
    }
}
