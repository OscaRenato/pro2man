﻿using System;

namespace Pro2Man.Common.Model
{
    public class WorkOrderResponse
    {
        public int Id { get; set; }

        public string ObservMan { get; set; }

        public DateTime StartOrder { get; set; }

        public DateTime StartOrderLocal => StartOrder.ToLocalTime();

        public DateTime FinishOrder { get; set; }

        public DateTime FinishOrderLocal => FinishOrder.ToLocalTime();

        public string Photo { get; set; }
    }
}
