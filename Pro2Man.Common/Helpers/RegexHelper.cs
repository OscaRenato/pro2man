﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace Pro2Man.Common.Helpers
{
    public class RegexHelper : IRegexHelper
    {
        public bool IsValidEmail(string emailAdress)
        {
            try
            {
                new MailAddress(emailAdress);
                return true;
            }
            catch (FormatException)
            {

                return false;
            }
        }
    }
}
