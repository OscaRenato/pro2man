﻿namespace Pro2Man.Common.Helpers
{
    public interface IRegexHelper
    {
        bool IsValidEmail(string emailAdress);
    }
}
