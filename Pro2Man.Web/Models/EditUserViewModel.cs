﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Pro2Man.Web.Models
{
    public class EditUserViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Nombre Operario")]
        [MaxLength(50, ErrorMessage = "El campo {0} no puede tener más de {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Name { get; set; }

        [Display(Name = "Apellido")]
        [MaxLength(50, ErrorMessage = "El campo {0} no puede tener más de {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string LastName { get; set; }

        [Display(Name = "DNI")]
        [MaxLength(8, ErrorMessage = "El campo {0} no puede tener más de {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Dni { get; set; }

        [Display(Name = "Celular")]
        [MaxLength(9, ErrorMessage = "El campo {0} no puede tener más de {1} caracteres.")]
        public string Phone { get; set; }

        [Display(Name = "Foto Operario")]
        public string Photo { get; set; }

        [Display(Name = "Foto Operario")]
        public IFormFile PhotoFile { get; set; }
    }
}
