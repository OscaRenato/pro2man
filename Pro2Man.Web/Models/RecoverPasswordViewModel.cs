﻿using System.ComponentModel.DataAnnotations;

namespace Pro2Man.Web.Models
{
    public class RecoverPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
