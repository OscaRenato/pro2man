﻿using System.ComponentModel.DataAnnotations;

namespace Pro2Man.Web.Models
{
    public class ChangePasswordViewModel
    {
        [Display(Name = "Contraseña Actual")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "El campo {0} debe tener entre {1} a {2} caracteres.")]
        public string OldPassword { get; set; }

        [Display(Name = "Contraseña Nueva")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "El campo {0} debe tener entre {1} a {2} caracteres.")]
        public string NewPassword { get; set; }

        [Display(Name = "Confirmar Nueva Contraseña")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "El campo {0} debe tener entre {1} a {2} caracteres.")]
        [Compare("NewPassword")]
        public string PasswordConfirm { get; set; }
    }
}
