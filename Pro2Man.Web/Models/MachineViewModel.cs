﻿using Microsoft.AspNetCore.Http;
using Pro2Man.Web.Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace Pro2Man.Web.Models
{
    public class MachineViewModel : MachineEntity
    {
        [Display(Name = "Foto Equipo")]
        public IFormFile PhotoFile { get; set; }
    }
}
