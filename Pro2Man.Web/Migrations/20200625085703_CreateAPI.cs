﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Pro2Man.Web.Migrations
{
    public partial class CreateAPI : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkOrders_Reports_ReportId",
                table: "WorkOrders");

            migrationBuilder.DropIndex(
                name: "IX_WorkOrders_ReportId",
                table: "WorkOrders");

            migrationBuilder.DropColumn(
                name: "ReportId",
                table: "WorkOrders");

            migrationBuilder.RenameColumn(
                name: "StarOrder",
                table: "WorkOrders",
                newName: "StartOrder");

            migrationBuilder.AddColumn<int>(
                name: "WorkOrderId",
                table: "Reports",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Reports_WorkOrderId",
                table: "Reports",
                column: "WorkOrderId",
                unique: true,
                filter: "[WorkOrderId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_WorkOrders_WorkOrderId",
                table: "Reports",
                column: "WorkOrderId",
                principalTable: "WorkOrders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reports_WorkOrders_WorkOrderId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Reports_WorkOrderId",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "WorkOrderId",
                table: "Reports");

            migrationBuilder.RenameColumn(
                name: "StartOrder",
                table: "WorkOrders",
                newName: "StarOrder");

            migrationBuilder.AddColumn<int>(
                name: "ReportId",
                table: "WorkOrders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_WorkOrders_ReportId",
                table: "WorkOrders",
                column: "ReportId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkOrders_Reports_ReportId",
                table: "WorkOrders",
                column: "ReportId",
                principalTable: "Reports",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
