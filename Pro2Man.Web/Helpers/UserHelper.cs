﻿using Microsoft.AspNetCore.Identity;
using Pro2Man.Common.Enums;
using Pro2Man.Web.Data.Entities;
using Pro2Man.Web.Models;
using System;
using System.Threading.Tasks;

namespace Pro2Man.Web.Helpers
{
    public class UserHelper : IUserHelper
    {
        private readonly UserManager<OperatorEntity> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<OperatorEntity> _signInManager;

        public UserHelper(
            UserManager<OperatorEntity> userManager,
            RoleManager<IdentityRole> roleManager,
            SignInManager<OperatorEntity> signInManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
        }

        public async Task<string> GeneratePasswordResetTokenAsync(OperatorEntity user)
        {
            return await _userManager.GeneratePasswordResetTokenAsync(user);
        }

        public async Task<IdentityResult> ResetPasswordAsync(OperatorEntity user, string token, string password)
        {
            return await _userManager.ResetPasswordAsync(user, token, password);
        }

        public async Task<IdentityResult> ConfirmEmailAsync(OperatorEntity user, string token)
        {
            return await _userManager.ConfirmEmailAsync(user, token);
        }

        public async Task<string> GenerateEmailConfirmationTokenAsync(OperatorEntity user)
        {
            return await _userManager.GenerateEmailConfirmationTokenAsync(user);
        }

        public async Task<OperatorEntity> AddUserAsync(AddUserViewModel model, string path)
        {
            OperatorEntity operatorEntity = new OperatorEntity
            {
                Name = model.Name,
                LastName = model.LastName,
                Dni = model.Dni,
                CodeOperator = model.CodeOperator,
                Email = model.UserName,
                UserName = model.UserName,
                PhoneNumber = model.Phone,
                Photo = path,
                Occupation = model.OperatorTypeId == 1 ? UserType.Production : UserType.Maintenance,
            };

            IdentityResult result = await _userManager.CreateAsync(operatorEntity, model.Password);
            if (result != IdentityResult.Success)
            {
                return null;
            }

            OperatorEntity newOperator = await GetUserAsync(model.UserName);
            await AddUserToRoleAsync(newOperator, operatorEntity.Occupation.ToString());
            return newOperator;
        }

        public async Task<IdentityResult> AddUserAsync(OperatorEntity user, string password)
        {
            return await _userManager.CreateAsync(user, password);
        }

        public async Task AddUserToRoleAsync(OperatorEntity user, string roleName)
        {
            await _userManager.AddToRoleAsync(user, roleName);
        }

        public async Task<IdentityResult> ChangePasswordAsync(OperatorEntity user, string oldPassword, string newPassword)
        {
            return await _userManager.ChangePasswordAsync(user, oldPassword, newPassword);
        }

        public async Task<IdentityResult> UpDateUserAsync(OperatorEntity user)
        {
            return await _userManager.UpdateAsync(user);
        }

        public async Task CheckRoleAsync(string roleName)
        {
            bool roleExists = await _roleManager.RoleExistsAsync(roleName);
            if (!roleExists)
            {
                await _roleManager.CreateAsync(new IdentityRole
                {
                    Name = roleName
                });
            }
        }

        public async Task<OperatorEntity> GetUserAsync(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }

        public async Task<OperatorEntity> GetUserAsync(Guid userId)
        {
            return await _userManager.FindByIdAsync(userId.ToString());
        }

        public async Task<bool> IsUserInRoleAsync(OperatorEntity user, string roleName)
        {
            return await _userManager.IsInRoleAsync(user, roleName);
        }

        public async Task<SignInResult> LoginAsync(LoginViewModel model)
        {
            return await _signInManager.PasswordSignInAsync(
                model.Username,
                model.Password,
                model.RememberMe,
                false);
        }

        public async Task LogoutAsync()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<SignInResult> ValidatePasswordAsync(OperatorEntity user, string password)
        {
            return await _signInManager.CheckPasswordSignInAsync(user, password, false);
        }
    }
}
