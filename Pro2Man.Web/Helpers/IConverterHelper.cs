﻿using Pro2Man.Common.Model;
using Pro2Man.Web.Data.Entities;
using Pro2Man.Web.Models;
using System.Collections.Generic;


namespace Pro2Man.Web.Helpers
{
    public interface IConverterHelper
    {
        MachineEntity ToMachineEntity(MachineViewModel model, string path, bool isNew);

        MachineViewModel ToMachineViewModel(MachineEntity machineEntity);        

        OperatorResponse ToOperatorResponse(OperatorEntity operatorEntity);

        List<OperatorResponse> ToOperatorResponse(List<OperatorEntity> operatorEntities);

        ReportResponse ToReportResponse(ReportEntity reportEntity);

        List<ReportResponse> ToReportResponse(List<ReportEntity> reportEntities);
    }
}
