﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Pro2Man.Web.Data;
using System.Collections.Generic;

namespace Pro2Man.Web.Helpers
{
    public class CombosHelper : ICombosHelper
    {
        private readonly DataContext _context;

        public CombosHelper(DataContext context)
        {
            _context = context;
        }

        public IEnumerable<SelectListItem> GetComboRoles()
        {
            List<SelectListItem> list = new List<SelectListItem>
            {
                new SelectListItem{Value = "0", Text = "[Selecciona un Cargo...]"},
                new SelectListItem{Value = "1", Text = "Producción"},
                new SelectListItem{Value = "2", Text = "Mantenimiento"}
            };

            return list;
        }
    }
}
