﻿using Microsoft.AspNetCore.Identity;
using Pro2Man.Web.Data.Entities;
using Pro2Man.Web.Models;
using System;
using System.Threading.Tasks;

namespace Pro2Man.Web.Helpers
{
    public interface IUserHelper
    {
        Task<string> GeneratePasswordResetTokenAsync(OperatorEntity user);

        Task<IdentityResult> ResetPasswordAsync(OperatorEntity user, string token, string password);

        Task<string> GenerateEmailConfirmationTokenAsync(OperatorEntity user);

        Task<IdentityResult> ConfirmEmailAsync(OperatorEntity user, string token);

        Task<OperatorEntity> AddUserAsync(AddUserViewModel model, string path);

        Task<OperatorEntity> GetUserAsync(string email);

        Task<OperatorEntity> GetUserAsync(Guid userId);

        Task<IdentityResult> AddUserAsync(OperatorEntity user, string password);

        Task<IdentityResult> ChangePasswordAsync(OperatorEntity user, string oldPassword, string newPassword);

        Task<IdentityResult> UpDateUserAsync(OperatorEntity user);

        Task CheckRoleAsync(string roleName);

        Task AddUserToRoleAsync(OperatorEntity user, string roleName);

        Task<bool> IsUserInRoleAsync(OperatorEntity user, string roleName);

        Task<SignInResult> LoginAsync(LoginViewModel model);

        Task<SignInResult> ValidatePasswordAsync(OperatorEntity user, string password);

        Task LogoutAsync();
    }
}
