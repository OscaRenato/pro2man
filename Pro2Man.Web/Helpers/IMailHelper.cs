﻿using Pro2Man.Common.Models;

namespace Pro2Man.Web.Helpers
{
    public interface IMailHelper
    {
        Response SendMail(string to, string subject, string body);
    }
}
