﻿using Pro2Man.Common.Model;
using Pro2Man.Web.Data.Entities;
using Pro2Man.Web.Models;
using System.Collections.Generic;
using System.Linq;

namespace Pro2Man.Web.Helpers
{
    public class ConverterHelper : IConverterHelper
    {
        public MachineEntity ToMachineEntity(MachineViewModel model, string path, bool isNew)
        {
            return new MachineEntity
            {
                Id = isNew ? 0 : model.Id,
                Photo = path,
                NameMachine = model.NameMachine,
                CodeMachine = model.CodeMachine
            };
        }

        public MachineViewModel ToMachineViewModel(MachineEntity machineEntity)
        {
            return new MachineViewModel
            {
                Id = machineEntity.Id,
                Photo = machineEntity.Photo,
                NameMachine = machineEntity.NameMachine,
                CodeMachine = machineEntity.CodeMachine
            };
        }

        //Operator-Report-WorkOrder
        //Operator-WorkOrder
        public OperatorResponse ToOperatorResponse(OperatorEntity operatorEntity)
        {
            return new OperatorResponse
            {
                Id = operatorEntity.Id,
                Name = operatorEntity.Name,
                LastName = operatorEntity.LastName,
                Dni = operatorEntity.Dni,
                CodeOperator = operatorEntity.CodeOperator,
                Occupation = operatorEntity.Occupation,
                Photo = operatorEntity.Photo,
                Email = operatorEntity.Email,
                Phone = operatorEntity.PhoneNumber,
                Reports = operatorEntity.Reports?.Select(r => new ReportResponse
                {
                    Id = r.Id,
                    ObservPro = r.ObservPro,
                    Status = r.Status,
                    StarReport = r.StarReport,                                      
                    WorkOrder = ToWorkOrderResponse(r.WorkOrder),
                    Machine = ToMachineResponse(r.Machine),
                }).ToList(),
                WorkOrders = operatorEntity.WorkOrders?.Select(w => new WorkOrderResponse
                {
                    Id = w.Id,
                    ObservMan = w.ObservMan,
                    StartOrder = w.StartOrder,
                    FinishOrder = w.FinishOrder,                    
                    Photo = w.Photo,
                }).ToList(),
            };
        }

        private MachineResponse ToMachineResponse(MachineEntity machineEntity)
        {
            return new MachineResponse
            {
                Id = machineEntity.Id,
                CodeMachine = machineEntity.CodeMachine,
                NameMachine = machineEntity.NameMachine,
                Photo = machineEntity.Photo,
            };
        }

        public ReportResponse ToReportResponse(ReportEntity reportEntity)
        {
            return new ReportResponse
            {
                Id = reportEntity.Id,
                StarReport = reportEntity.StarReport,
                Status = reportEntity.Status,
                ObservPro = reportEntity.ObservPro,
                WorkOrder = ToWorkOrderResponse(reportEntity.WorkOrder),
            };
        }

        // Lista Operators
        public List<OperatorResponse> ToOperatorResponse(List<OperatorEntity> operatorEntities)
        {
            List<OperatorResponse> list = new List<OperatorResponse>();
            foreach (OperatorEntity operatorEntity in operatorEntities)
            {
                list.Add(ToOperatorResponse(operatorEntity));
            }
            return list;
        }

        //Lista Reports
        public List<ReportResponse> ToReportResponse(List<ReportEntity> reportEntities)
        {
            List<ReportResponse> list = new List<ReportResponse>();
            foreach (ReportEntity reportEntity in reportEntities)
            {
                list.Add(ToReportResponse(reportEntity));
            }
            return list;
        }

        //Metodo WorkOrder Response
        private WorkOrderResponse ToWorkOrderResponse(WorkOrderEntity workOrder)
        {
            if (workOrder == null)
            {
                return null;
            }

            return new WorkOrderResponse
            {
                Id = workOrder.Id,
                StartOrder = workOrder.StartOrder,
                FinishOrder = workOrder.FinishOrder,
                ObservMan = workOrder.ObservMan,
                Photo = workOrder.Photo,
            };
        }
    }
}
