﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Pro2Man.Web.Data.Entities
{
    public class ReportEntity
    {
        public int Id { get; set; }

        [Display(Name = "Observación Produccion")]
        [MaxLength(150, ErrorMessage = "El campo {0} no puede tener más de {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string ObservPro { get; set; }

        [Display(Name = "Estado")]
        public string Status { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Fecha de Reporte")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd hh:mm}", ApplyFormatInEditMode = false)]
        public DateTime StarReport { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Fecha de Reporte")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd hh:mm}", ApplyFormatInEditMode = false)]
        public DateTime StartReportLocal => StarReport.ToLocalTime();

        public int? WorkOrderId { get; set; }

        public MachineEntity Machine { get; set; }

        public OperatorEntity Operator { get; set; }

        public WorkOrderEntity WorkOrder { get; set; }
    }
}
