﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Pro2Man.Web.Data.Entities
{
    public class WorkOrderEntity
    {
        public int Id { get; set; }

        [Display(Name = "Observación Mantenimiento")]
        [MaxLength(150, ErrorMessage = "El campo {0} no puede tener más de {1} caracteres.")]        
        public string ObservMan { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Fecha de Inicio de Mantenimiento")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd hh:mm}", ApplyFormatInEditMode = false)]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public DateTime StartOrder { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Fecha de Inicio de Mantenimiento")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd hh:mm}", ApplyFormatInEditMode = false)]
        public DateTime StartOrderLocal => StartOrder.ToLocalTime();

        [DataType(DataType.DateTime)]
        [Display(Name = "Fecha de Fin de Mantenimiento")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd hh:mm}", ApplyFormatInEditMode = false)]
        public DateTime FinishOrder { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Fecha de Fin de Mantenimiento")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd hh:mm}", ApplyFormatInEditMode = false)]
        public DateTime FinishOrderLocal => FinishOrder.ToLocalTime();

        [Display(Name = "Foto Equipo Arreglado")]
        public string Photo { get; set; }        

        public ReportEntity Report { get; set; }

        public OperatorEntity Operator { get; set; }
    }
}
