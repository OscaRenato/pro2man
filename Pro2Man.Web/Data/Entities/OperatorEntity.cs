﻿using Microsoft.AspNetCore.Identity;
using Pro2Man.Common.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Pro2Man.Web.Data.Entities
{
    public class OperatorEntity : IdentityUser
    {
        [Display(Name = "Nombre Operario")]
        [MaxLength(50, ErrorMessage = "El campo {0} no puede tener más de {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Name { get; set; }

        [Display(Name = "Apellido")]
        [MaxLength(50, ErrorMessage = "El campo {0} no puede tener más de {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string LastName { get; set; }

        [Display(Name = "DNI")]
        [MaxLength(8, ErrorMessage = "El campo {0} no puede tener más de {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Dni { get; set; }

        [Display(Name = "Codigo Operario")]
        [MaxLength(10, ErrorMessage = "El campo {0} no puede tener más de {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string CodeOperator { get; set; }

        [Display(Name = "Cargo Operario")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public UserType Occupation { get; set; }

        [Display(Name = "Foto Operario")]
        public string Photo { get; set; }

        [Display(Name = "Foto Operario")]
        public string FullPhoto => string.IsNullOrEmpty(Photo)
            ? "https://pro2manwebservice.azurewebsites.net/images/Users/NoImage.png"
            : $"https://pro2manwebservice.azurewebsites.net/{Photo.Substring(1)}";

        [Display(Name = "Nombre Operario")]
        public string FullName => $"{Name} {LastName}";

        public ICollection<ReportEntity> Reports { get; set; }

        public ICollection<WorkOrderEntity> WorkOrders { get; set; }
    }
}
