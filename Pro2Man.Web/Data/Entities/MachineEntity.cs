﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pro2Man.Web.Data.Entities
{
    public class MachineEntity
    {
        public int Id { get; set; }

        [Display(Name = "Codigo Equipo")]
        [MaxLength(6, ErrorMessage = "El campo {0} no puede tener más de {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string CodeMachine { get; set; }

        [Display(Name = "Nombre Equipo")]
        [MaxLength(50, ErrorMessage = "El campo {0} no puede tener más de {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string NameMachine { get; set; }

        [Display(Name = "Foto Equipo")]
        public string Photo { get; set; }

        [Display(Name = "Foto Equipo")]
        public string FullPhoto => string.IsNullOrEmpty(Photo)
            ? "~/images/Users/NoImage.png"
            : $"https://pro2manwebservice.azurewebsites.net{Photo.Substring(1)}";

        public ICollection<ReportEntity> Reports { get; set; }
    }
}
