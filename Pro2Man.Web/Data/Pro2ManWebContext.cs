﻿using Microsoft.EntityFrameworkCore;

namespace Pro2Man.Web.Data
{
    public class Pro2ManWebContext : DbContext
    {
        public Pro2ManWebContext(DbContextOptions<Pro2ManWebContext> options)
            : base(options)
        {
        }

        public DbSet<Pro2Man.Web.Data.Entities.MachineEntity> MachineEntity { get; set; }
    }
}
