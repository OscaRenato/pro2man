﻿using Microsoft.EntityFrameworkCore.Internal;
using Pro2Man.Common.Enums;
using Pro2Man.Web.Data.Entities;
using Pro2Man.Web.Helpers;
using System.Linq;
using System.Threading.Tasks;

namespace Pro2Man.Web.Data
{
    public class SeedDb
    {
        private readonly DataContext _context;
        private readonly IUserHelper _userHelper;

        public SeedDb(DataContext context, IUserHelper userHelper)
        {
            _context = context;
            _userHelper = userHelper;
        }

        public async Task SeedAsync()
        {
            await _context.Database.EnsureCreatedAsync();
            await CheckRolesAsync();
            await CheckMachineAsync();
            await CheckUserAsync("Oscar", "Canales", "46369029", "654321", UserType.Admin, "o.canales@pucp.pe", "984976560");
            await CheckUserAsync("Gerson", "Carranza", "58749658", "654322", UserType.Production, "g.carranza@pucp.pe", "935873049");
            await CheckUserAsync("Renato", "Cordova", "85479658", "654323", UserType.Maintenance, "oscarenato.15@gmail.com", "984976560");
            
        }

        private async Task CheckRolesAsync()
        {
            await _userHelper.CheckRoleAsync(UserType.Admin.ToString());
            await _userHelper.CheckRoleAsync(UserType.Maintenance.ToString());
            await _userHelper.CheckRoleAsync(UserType.Production.ToString());
        }

        private async Task CheckMachineAsync()
        {
            if (!_context.Machines.Any())
            {
                AddMachine("000001", "Agitador");
                AddMachine("000002", "Tanque");
                AddMachine("000003", "Bomba");
                AddMachine("000004", "Tablero");
                AddMachine("000005", "Dosificador");
                AddMachine("000006", "Serpentin");
                AddMachine("000007", "Electrobomba");
                AddMachine("000008", "Reductor");
                AddMachine("000009", "Intercambiador");
                AddMachine("000010", "Motor");
                AddMachine("000011", "Molino");
                await _context.SaveChangesAsync();
            }
        }

        private async Task<OperatorEntity> CheckUserAsync(string name, string lastName, string dni, string codeOperator, UserType occupation, string email, string phone)
        {
            OperatorEntity user = await _userHelper.GetUserAsync(email);
            if (user == null)
            {
                user = new OperatorEntity
                {
                    Name = name,
                    LastName = lastName,
                    Dni = dni,
                    CodeOperator = codeOperator,
                    Occupation = occupation,
                    Email = email,
                    UserName = email,
                    PhoneNumber = phone,
                };

                await _userHelper.AddUserAsync(user, "123456");
                await _userHelper.AddUserToRoleAsync(user, occupation.ToString());

                var token = await _userHelper.GenerateEmailConfirmationTokenAsync(user);
                await _userHelper.ConfirmEmailAsync(user, token);
            }

            return user;
        }

        private void AddMachine(string code, string name)
        {
            _context.Machines.Add(new MachineEntity
            {
                CodeMachine = code,
                NameMachine = name,
                Photo = $"~/images/Machines/{name}.jpg"
            });
        }
    }
}
