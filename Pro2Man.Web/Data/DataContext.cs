﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Pro2Man.Web.Data.Entities;

namespace Pro2Man.Web.Data
{
    public class DataContext : IdentityDbContext<OperatorEntity>
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<MachineEntity> Machines { get; set; }        

        public DbSet<ReportEntity> Reports { get; set; }

        public DbSet<WorkOrderEntity> WorkOrders { get; set; }

        public DbSet<OperatorEntity> Operators { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<MachineEntity>()
                .HasIndex(t => t.CodeMachine)
                .IsUnique();
        }
    }
}
