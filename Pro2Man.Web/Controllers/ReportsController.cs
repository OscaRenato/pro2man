﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pro2Man.Web.Data;
using Pro2Man.Web.Data.Entities;
using Pro2Man.Web.Helpers;
using System.Linq;
using System.Threading.Tasks;

namespace Pro2Man.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ReportsController : Controller
    {
        private readonly DataContext _context;
        private readonly IImageHelper _imageHelper;
        private readonly IConverterHelper _converterHelper;

        public ReportsController(
            DataContext context,
            IImageHelper imageHelper,
            IConverterHelper converterHelper)
        {
            _context = context;
            _imageHelper = imageHelper;
            _converterHelper = converterHelper;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _context.Reports
                .Include(t => t.Operator)
                .Include(t => t.Machine)
                .Include(t => t.WorkOrder)
                .OrderBy(t => t.StarReport)
                .ToListAsync());
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ReportEntity reportEntity = await _context.Reports
                .FirstOrDefaultAsync(m => m.Id == id);
            if (reportEntity == null)
            {
                return NotFound();
            }

            _context.Reports.Remove(reportEntity);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ReportEntity reportEntity = await _context.Reports
                .Include(t => t.Operator)
                .Include(t => t.Machine)
                .Include(t => t.WorkOrder)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (reportEntity == null)
            {
                return NotFound();
            }

            return View(reportEntity);
        }

        public async Task<IActionResult> WorkOrder(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ReportEntity workOrderEntity = await _context.Reports
                .Include(t => t.WorkOrder)
                .ThenInclude(t => t.Operator)
                .Include(t => t.Machine)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (workOrderEntity == null)
            {
                return NotFound();
            }

            return View(workOrderEntity);
        }
    }
}
