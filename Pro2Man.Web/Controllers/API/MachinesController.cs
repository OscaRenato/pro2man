﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pro2Man.Web.Data;
using Pro2Man.Web.Data.Entities;
using System.Threading.Tasks;

namespace Pro2Man.Web.Controllers.API
{
    [ApiController]
    [Route("api/[controller]")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MachinesController : ControllerBase
    {
        private readonly DataContext _context;

        public MachinesController(DataContext context)
        {
            _context = context;
        }

        [HttpGet("{code}")]
        public async Task<IActionResult> GetMachinesbyCode([FromRoute] string code)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            MachineEntity machineEntity = await _context.Machines.FirstOrDefaultAsync(m => m.CodeMachine == code);

            if (machineEntity == null)
            {
                return NotFound();
            }

            return Ok(machineEntity);
        }
    }
}
