﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pro2Man.Common.Models;
using Pro2Man.Web.Data;
using Pro2Man.Web.Data.Entities;
using Pro2Man.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pro2Man.Web.Controllers.API
{
    [Route("api/[controller]")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class ReportsController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IConverterHelper _converterHelper;
        private readonly IUserHelper _userHelper;

        public ReportsController(
            DataContext context,
            IConverterHelper converterHelper,
            IUserHelper userHelper)
        {
            _context = context;
            _converterHelper = converterHelper;
            _userHelper = userHelper;
        }

        [HttpGet]
        public async Task<IActionResult> GetReports()
        {
            List<ReportEntity> reports = await _context.Reports
                .Include(r => r.WorkOrder)
                .ToListAsync();
            return Ok(_converterHelper.ToReportResponse(reports));
        }

        //Crear un Reporte
        [HttpPost]
        public async Task<IActionResult> PostReportEntity([FromBody] ReportRequest reportRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }            

            MachineEntity machineEntity = await _context.Machines.FirstOrDefaultAsync(m => m.CodeMachine == reportRequest.CodeMachine);
            if (machineEntity == null)
            {
                return BadRequest("El código de máquina no existe o no es válido.");
            }

            OperatorEntity operatorEntity = await _userHelper.GetUserAsync(reportRequest.UserId);
            if (operatorEntity == null)
            {
                return BadRequest("El usuario nunca existe.");
            }

            WorkOrderEntity workOrderEntity = await _context.WorkOrders.FirstOrDefaultAsync(w => w.Operator.Id == reportRequest.IdOperator);
            if (workOrderEntity == null)
            {
                ReportEntity reportEntity = new ReportEntity
                {
                    ObservPro = reportRequest.ObservPro,
                    Status = "Pendiente",
                    StarReport = DateTime.UtcNow,
                    WorkOrderId = null,
                    //Operator = operatorEntity,
                    //Machine = machineEntity
                };
                _context.Reports.Add(reportEntity);
            }
            else
            {
                ReportEntity reportEntity = new ReportEntity
                {
                    ObservPro = reportRequest.ObservPro,
                    Status = "Pendiente",
                    StarReport = DateTime.UtcNow,
                    WorkOrderId = workOrderEntity.Id,
                };
                _context.Reports.Add(reportEntity);
            }            
            await _context.SaveChangesAsync();
            return Ok("Se ha creado el reporte");
        }
    }
}
