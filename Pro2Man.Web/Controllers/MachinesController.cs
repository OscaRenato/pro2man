﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pro2Man.Web.Data;
using Pro2Man.Web.Data.Entities;
using Pro2Man.Web.Helpers;
using Pro2Man.Web.Models;
using System;
using System.Threading.Tasks;

namespace Pro2Man.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class MachinesController : Controller
    {
        private readonly DataContext _context;
        private readonly IImageHelper _imageHelper;
        private readonly IConverterHelper _converterHelper;

        public MachinesController(
            DataContext context,
            IImageHelper imageHelper,
            IConverterHelper converterHelper)
        {
            _context = context;
            _imageHelper = imageHelper;
            _converterHelper = converterHelper;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _context.Machines.ToListAsync());
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MachineEntity machineEntity = await _context.Machines
                .FirstOrDefaultAsync(m => m.Id == id);
            if (machineEntity == null)
            {
                return NotFound();
            }

            return View(machineEntity);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MachineViewModel machineViewModel)
        {
            if (ModelState.IsValid)
            {
                string path = string.Empty;

                if (machineViewModel.PhotoFile != null)
                {
                    path = await _imageHelper.UploadImageAsync(machineViewModel.PhotoFile, "Machines");
                }

                MachineEntity machineEntity = _converterHelper.ToMachineEntity(machineViewModel, path, true);
                _context.Add(machineEntity);
                try
                {
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {

                    if (ex.InnerException.Message.Contains("duplicate"))
                    {
                        ModelState.AddModelError(string.Empty, "El codigo de equipo ya existe.");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, ex.InnerException.Message);
                    }
                }
            }
            return View(machineViewModel);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MachineEntity machineEntity = await _context.Machines.FindAsync(id);
            if (machineEntity == null)
            {
                return NotFound();
            }

            MachineViewModel machineViewModel = _converterHelper.ToMachineViewModel(machineEntity);
            return View(machineViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, MachineViewModel machineViewModel)
        {
            if (id != machineViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                string path = machineViewModel.Photo;

                if (machineViewModel.PhotoFile != null)
                {
                    path = await _imageHelper.UploadImageAsync(machineViewModel.PhotoFile, "Machines");
                }

                MachineEntity machineEntity = _converterHelper.ToMachineEntity(machineViewModel, path, false);
                _context.Update(machineEntity);
                try
                {
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {

                    if (ex.InnerException.Message.Contains("duplicate"))
                    {
                        ModelState.AddModelError(string.Empty, "El codigo de equipo ya existe.");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, ex.InnerException.Message);
                    }
                }
            }
            return View(machineViewModel);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            //<input type="submit" value="Delete" class="btn btn-default" />
            if (id == null)
            {
                return NotFound();
            }

            MachineEntity machineEntity = await _context.Machines
                .FirstOrDefaultAsync(m => m.Id == id);
            if (machineEntity == null)
            {
                return NotFound();
            }

            _context.Machines.Remove(machineEntity);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
