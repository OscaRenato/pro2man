﻿using Pro2Man.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pro2Man.Prism.Helpers
{
    public static class CombosHelper
    {
        public static List<Role> GetRoles()
        {
            return new List<Role>
            {
                new Role { Id = 1, Name = "Mantenimiento"},
                new Role { Id = 2, Name = "Producción"},
            };
        }
    }
}
