﻿using Prism.Navigation;
using Pro2Man.Common.Model;
using Pro2Man.Common.Models;
using Pro2Man.Common.Services;
using System.Collections.Generic;

namespace Pro2Man.Prism.ViewModels
{
    public class ListReportsPageViewModel : ViewModelBase
    {
        private readonly IApiService _apiService;
        private List<ReportResponse> _reports;

        public ListReportsPageViewModel(INavigationService navigationService, IApiService apiService) : base(navigationService)
        {
            _apiService = apiService;
            Title = "Lista de Reportes";
            LoadReportsAsync();
        }
        
        public List<ReportResponse> Reports
        {
            get => _reports;
            set => SetProperty(ref _reports, value);
        }

        private async void LoadReportsAsync()
        {            
            string url = App.Current.Resources["UrlAPI"].ToString();
            bool connection = await _apiService.CheckConnectionAsync(url);
            if (!connection)
            {                
                await App.Current.MainPage.DisplayAlert("Error", "Verifique conexión de internet", "Aceptar");
                return;
            }
            Response response = await _apiService.GetListAsync<ReportResponse>(url, "api", "/Reports");
            if (!response.IsSuccess)
            {
                await App.Current.MainPage.DisplayAlert("Error", response.Message, "Acept");
                return;
            }
            Reports = (List<ReportResponse>)response.Result;
        }
    }
}
