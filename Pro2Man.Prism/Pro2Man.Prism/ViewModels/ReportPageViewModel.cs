﻿using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using Pro2Man.Common.Helpers;
using Pro2Man.Common.Model;
using Pro2Man.Common.Models;
using Pro2Man.Common.Services;
using System;
using System.Threading.Tasks;

namespace Pro2Man.Prism.ViewModels
{
    public class ReportPageViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private MachineResponse _machine;
        private bool _isEnabled;
        private bool _isRunning;
        private string _observPro;
        private DelegateCommand _checkCodeCommand;
        private DelegateCommand _createReportCommand;


        public ReportPageViewModel(INavigationService navigationService, IApiService apiService) : base(navigationService)
        {
            _navigationService = navigationService;
            _apiService = apiService;
            Title = "Crear Reporte";
            IsEnabled = true;
        }

        public DelegateCommand CreateReportCommand => _createReportCommand ?? (_createReportCommand = new DelegateCommand(CreateReportAsync));        

        public DelegateCommand CheckCodeCommand => _checkCodeCommand ?? (_checkCodeCommand = new DelegateCommand(CheckCodeAsync));

        public bool IsEnabled
        {
            get => _isEnabled;
            set => SetProperty(ref _isEnabled, value);
        }

        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public string ObservPro
        {
            get => _observPro;
            set => SetProperty(ref _observPro, value);
        }

        public MachineResponse Machine
        {
            get => _machine;
            set => SetProperty(ref _machine, value);
        }        

        public string CodeMachine { get; set; }        

        private async void CheckCodeAsync()
        {
            if (string.IsNullOrEmpty(CodeMachine))
            {
                IsRunning = false;
                await App.Current.MainPage.DisplayAlert(
                    "Error",
                    "Debe ingresar un Código de Equipo",
                    "Aceptar");
                return;
            }

            IsRunning = true;
            string url = App.Current.Resources["UrlAPI"].ToString();
            bool connection = await _apiService.CheckConnectionAsync(url);
            if (!connection)
            {
                IsRunning = false;
                await App.Current.MainPage.DisplayAlert("Error", "Verifique conexión de internet", "Aceptar");
                return;
            }

            Response response = await _apiService.GetMachineAsync(CodeMachine, url, "api", "/Machines");
            IsRunning = false;
            if (!response.IsSuccess)
            {
                await App.Current.MainPage.DisplayAlert(
                    "Error",
                    "El Código no existe",
                    "Aceptar");
                return;
            }

            Machine = (MachineResponse)response.Result;
        }

        private async void CreateReportAsync()
        {
            bool isValid = await ValidateDataAsync();
            if (!isValid)
            {
                return;
            }

            IsRunning = true;
            IsEnabled = false;

            string url = App.Current.Resources["UrlAPI"].ToString();
            bool connection = await _apiService.CheckConnectionAsync(url);
            if (!connection)
            {
                IsRunning = false;
                IsEnabled = true;
                await App.Current.MainPage.DisplayAlert("Error", "Error de conexión", "Aceptar");
                return;
            }

            OperatorResponse user = JsonConvert.DeserializeObject<OperatorResponse>(Settings.User);
            TokenResponse token = JsonConvert.DeserializeObject<TokenResponse>(Settings.Token);

            ReportRequest reportRequest = new ReportRequest
            {
                ObservPro = ObservPro,
                CodeMachine = Machine.CodeMachine,
                UserId = new Guid(user.Id),
                IdOperator = user.Id,
            };
            
            Response response = await _apiService.NewReportAsync(url, "/api", "/Reports", reportRequest, "bearer", token.Token);
            
            if (!response.IsSuccess)
            {
                IsRunning = false;
                IsEnabled = true;
                await App.Current.MainPage.DisplayAlert("Error", response.Message, "Accept");
                return;
            }
                        
            IsRunning = false;
            IsEnabled = true;
            await App.Current.MainPage.DisplayAlert("OK", "Reporte realizado", "Aceptar");
            await _navigationService.GoBackAsync();
        }

        private async Task<bool> ValidateDataAsync()
        {
            if (string.IsNullOrEmpty(CodeMachine))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe de poner un Código", "Aceptar");
                return false;
            }

            if (string.IsNullOrEmpty(ObservPro))
            {
                await App.Current.MainPage.DisplayAlert("Error", "Debe de poner una Observación", "Aceptar");
                return false;
            }

            return true;
        }
    }
}
