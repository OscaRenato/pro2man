﻿using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using Pro2Man.Common.Enums;
using Pro2Man.Common.Helpers;
using Pro2Man.Common.Model;
using Pro2Man.Common.Models;
using Pro2Man.Common.Services;
using Pro2Man.Prism.Views;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Pro2Man.Prism.ViewModels
{
    public class Pro2ManMasterDetailPageViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private OperatorResponse _user;
        private DelegateCommand _modifyUserCommand;
        private static Pro2ManMasterDetailPageViewModel _instance;

        public Pro2ManMasterDetailPageViewModel(INavigationService navigationService, IApiService apiService) : base(navigationService)
        {
            _instance = this;
            _navigationService = navigationService;
            _apiService = apiService;
            LoadUser();
            LoadMenus();
        }

        public DelegateCommand ModifyUserCommand => _modifyUserCommand ?? (_modifyUserCommand = new DelegateCommand(ModifyUserAsync));

        public OperatorResponse User
        {
            get => _user;
            set => SetProperty(ref _user, value);
        }

        public static Pro2ManMasterDetailPageViewModel GetInstance()
        {
            return _instance;
        }

        public async void ReloadUser()
        {
            string url = App.Current.Resources["UrlAPI"].ToString();
            bool connection = await _apiService.CheckConnectionAsync(url);
            if (!connection)
            {
                return;
            }

            OperatorResponse user = JsonConvert.DeserializeObject<OperatorResponse>(Settings.User);
            TokenResponse token = JsonConvert.DeserializeObject<TokenResponse>(Settings.Token);
            EmailRequest emailRequest = new EmailRequest
            {
                Email = user.Email,
            };

            Response response = await _apiService.GetUserByEmail(url, "api", "/Account/GetUserByEmail", "bearer", token.Token, emailRequest);
            OperatorResponse userResponse = (OperatorResponse)response.Result;
            Settings.User = JsonConvert.SerializeObject(userResponse);
            LoadUser();
        }

        private void LoadUser()
        {
            if (Settings.IsLogin)
            {
                User = JsonConvert.DeserializeObject<OperatorResponse>(Settings.User);
            }
        }

        private async void ModifyUserAsync()
        {
            await _navigationService.NavigateAsync($"/Pro2ManMasterDetailPage/NavigationPage/{nameof(ModifyUserPage)}");
        }

        public ObservableCollection<MenuItemViewModel> Menus { get; set; }

        private void LoadMenus()
        {
            List<Menu> menus = new List<Menu>
            {
                new Menu
                {
                    Icon = "ic_exit_to_app",
                    PageName = "LoginPage",
                    Title = "Cerrar Sesión",
                },

                new Menu
                {
                    Icon = "ic_account_circle",
                    PageName = "ModifyUserPage",
                    Title = "Cambiar Datos",
                },

                new Menu
                {
                    Icon = "ic_security",
                    PageName = "ChangePasswordPage",
                    Title = "Cambiar Contraseña",
                },

                new Menu
                {
                    Icon = "ic_format_list_bulleted",
                    PageName = "ListReportsPage",
                    Title = "Lista de Reportes",
                },

                new Menu
                {
                    Icon = User.Occupation == UserType.Production ? "ic_assignment" : "",
                    PageName = User.Occupation == UserType.Production ? "ReportPage" : "ListReportsPage",
                    Title = User.Occupation == UserType.Production ? "Reportar" : "",
                }
            };

            Menus = new ObservableCollection<MenuItemViewModel>(menus.Select(m => new MenuItemViewModel(_navigationService)
            {
                Icon = m.Icon,
                PageName = m.PageName,
                Title = m.Title
            }).ToList());
        }
    }
}
