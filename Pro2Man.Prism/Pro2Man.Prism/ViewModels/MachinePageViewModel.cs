﻿using Prism.Navigation;
using Pro2Man.Common.Model;
using Pro2Man.Common.Models;
using Pro2Man.Common.Services;
using System.Collections.Generic;

namespace Pro2Man.Prism.ViewModels
{
    public class MachinePageViewModel : ViewModelBase
    {
        private readonly IApiService _apiService;
        private List<MachineResponse> _machines;

        public MachinePageViewModel(INavigationService navigationService, IApiService apiService) : base(navigationService)
        {
            _apiService = apiService;
            Title = "Equipos en la Planta";            
            LoadMachinesAsync();
        }

        public List<MachineResponse> Machines
        {
            get => _machines;
            set => SetProperty(ref _machines, value);
        }

        private async void LoadMachinesAsync()
        {
            string url = App.Current.Resources["UrlAPI"].ToString();
            Response response = await _apiService.GetListAsync<MachineResponse>(url, "api", "/Machines");
            if (!response.IsSuccess)
            {
                await App.Current.MainPage.DisplayAlert("Error", response.Message, "Acept");
                return;
            }
            Machines = (List<MachineResponse>)response.Result;
        }
    }
}
